import json
from abc import abstractmethod
from datetime import datetime, timedelta

from psutil import virtual_memory, cpu_freq, cpu_times, disk_usage, cpu_times_percent


class Resource:
    resources = {}

    def __init__(self, throttle: float = 5):
        self._update_interval = timedelta(seconds=throttle)
        self._prev_value = None
        self._last_update = datetime.min

    def __init_subclass__(cls, **kwargs):
        cls.resources[cls.__name__.lower().replace('resource', '')] = cls()

    @abstractmethod
    def _get_value(self):
        raise NotImplementedError()

    def get_value(self):
        if datetime.now() - self._last_update <= self._update_interval:
            return self._prev_value
        self._prev_value = self._get_value()
        self._last_update = datetime.now()
        return self._prev_value

    @classmethod
    def get_values(cls, resources):
        return {res_name: cls.resources[res_name].get_value() for res_name in resources}


class RamResource(Resource):
    def _get_value(self):
        return virtual_memory()._asdict()


class CpuResource(Resource):
    def _get_value(self):
        res = cpu_times_percent(0.25)._asdict()
        res['freq'] = cpu_freq().current
        res['freq_per_cpu'] = [x.current for x in cpu_freq(True)]
        return res


class DiskResource(Resource):
    def __init__(self, path='/', throttle=10):
        super().__init__(throttle=throttle)
        self._path = path

    def _get_value(self):
        return disk_usage(self._path)._asdict()
