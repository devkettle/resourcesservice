import asyncio
import json
import logging
from asyncio import gather
from datetime import datetime
from urllib.parse import urlencode

import aiohttp

_workers_number = 0


async def client_worker(
    session: aiohttp.ClientSession, resources: [str], interval=1, n=32, delta=0.5
):
    global _workers_number
    worker_id = _workers_number
    _workers_number += 1
    ws = await session.ws_connect(
        'ws://0.0.0.0:8080/api/resources?'
        + urlencode([('interval', interval), *(('resource', res) for res in resources)])
    )
    print('Worker', worker_id, 'started')
    print('Worker', worker_id, f'params: resources={resources}, interval={interval}')

    prev_time = None
    for _ in range(n):
        msg = await ws.receive()
        now = datetime.now()

        if msg.type == aiohttp.WSMsgType.text:
            assert not prev_time or (
                interval - delta
                <= (now - prev_time).total_seconds()
                <= interval + delta
            )
            data = json.loads(msg.data)
            print('Worker', worker_id, 'got', data)
            assert all(res in data for res in resources)
        elif msg.type in (
            aiohttp.WSMsgType.closed,
            msg.type == aiohttp.WSMsgType.error,
        ):
            print('Worker', worker_id, 'faced problems')
            break

        prev_time = now

    await ws.send_str('close')
    print('Worker', worker_id, 'finished')


async def main():
    async with aiohttp.ClientSession() as sess:
        await gather(
            *(
                client_worker(sess, resources, interval)
                for resources, interval in [
                    (['cpu', 'ram', 'disk'], 5),
                    (['disk'], 4),
                    (['cpu', 'ram'], 3),
                    (['disk', 'ram'], 2),
                    (['ram'], 1),
                ]
            )
        )


if __name__ == '__main__':
    asyncio.get_event_loop().run_until_complete(main())
